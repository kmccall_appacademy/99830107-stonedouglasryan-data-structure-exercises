# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.sort[-1] - arr.sort[0]
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  arr = str.downcase.split("")
  arr.count("a") + arr.count("e") + arr.count("i") + arr.count("o") +arr.count("u")
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  arr = str.downcase.split("")
  arr.delete("a")
  arr.delete("e")
  arr.delete("i")
  arr.delete("o")
  arr.delete("u")
  arr.join("")
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  int.to_s.split("").sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  str.downcase.split("") != str.downcase.split("").uniq
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
part_one = arr[0..2]
part_one.unshift("(")
part_one.push(") ")

part_two = arr[3..5]
part_two.push("-")

part_three = arr[6..11]
phone_number = part_one.concat(part_two.concat(part_three))
phone_number.join
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  numbers = str.split(",")
  numArr = []
  numbers.each do |ele|
    numArr.push(ele.to_i)
  end
  range(numArr)

end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  if offset > arr.length
    offset %= arr.length
    arr.drop(offset) + arr.take(offset)
  elsif offset < arr.length && offset > 0
    arr.drop(offset) + arr.take(offset)
  elsif offset < 0
    arr.drop(arr.length + offset) + arr.take(arr.length + offset)
  end
end
